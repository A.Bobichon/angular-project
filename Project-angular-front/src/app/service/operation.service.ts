import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Operation } from '../Entity/operation';

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  private url = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  //method pour afficher toute les operations
  findAllOperation(): Observable<Operation[]> {
    return this.http.get<Operation[]>(this.url);
  }

  //ajouter une operations
  addOperation(operation):Observable<Operation> {
    return this.http.post<Operation>(this.url + 'operationMethod' , operation);
  }

  //supprime une operations
  delete(id):Observable<Operation> {
    return this.http.delete<Operation>(this.url + 'operationMethod' + id);
  }
}
