import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../Entity/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'http://localhost:8080/'

  constructor(private http:HttpClient) { }

  //method pour afficher tout les users
  findAllUser():Observable<User[]> {
      return this.http.get<User[]>(this.url);
  }

  //method pour s'inscrire
  addUser(user):Observable<User> {
      return this.http.post<User>(this.url + 'userMethod' , user);
  }
}
