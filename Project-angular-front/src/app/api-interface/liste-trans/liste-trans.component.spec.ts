import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeTransComponent } from './liste-trans.component';

describe('ListeTransComponent', () => {
  let component: ListeTransComponent;
  let fixture: ComponentFixture<ListeTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
