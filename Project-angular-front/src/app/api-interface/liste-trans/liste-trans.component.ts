import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Operation } from '../../Entity/operation';
import { OperationService } from '../../service/operation.service';

@Component({
  selector: 'app-liste-trans',
  templateUrl: './liste-trans.component.html',
  styleUrls: ['./liste-trans.component.css']
})
export class ListeTransComponent implements OnInit {

  operationsAff:Operation[] = [];

  constructor(private service:OperationService) { }

   //affiche les operations
   ngOnInit() {
     this.service.findAllOperation().subscribe( data => this.operationsAff = data);
  }

}
