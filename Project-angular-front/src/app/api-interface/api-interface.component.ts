import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Operation } from '../Entity/operation';
import { OperationService } from '../service/operation.service';

@Component({
  selector: 'app-api-interface',
  templateUrl: './api-interface.component.html',
  styleUrls: ['./api-interface.component.css']
})
export class ApiInterfaceComponent implements OnInit {
  compteur;
  constructor() { }

  ngOnInit() {
  }

  //method pour recevoir les donnée envoyer par les enfants
  parentsVal(event) {
    this.compteur = event;
  }
}
