import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Operation } from '../../Entity/operation';
import { Observable } from 'rxjs';
import { OperationService } from '../../service/operation.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  //output pour envoyer la valeur au parents
  @Output() calculDebit = new EventEmitter();

  operations: Observable<Operation[]>;
  operation: Operation = { montant: undefined, tag: undefined, datetime: undefined };

  signe:string; //le signe du calcul // changer le type

  constructor(private service:OperationService) { }

  //affiche les operations
  ngOnInit() {
    this.operations = this.service.findAllOperation();
  }

  //ajoute les operations
  add(operation:Operation){
    this.service.addOperation(operation).subscribe(() => this.ngOnInit());
  }

  //supprime les operation
  delete(id){
    this.service.delete(id).subscribe(() => this.ngOnInit());
    // reafecter la valeur supprimer

    //
  }

  //lance le calcul
    calcul(){

    }

  //envoye la valeur au parents
    calculResult(){
    //faire le calcul
    this.calculDebit.emit(this.operations);
  }



}
