import { Operation } from 'src/app/Entity/operation';

export interface User {
  id?:number;
  name:string;
  password:string;
  solde:number;
  operations?:Operation[];
}
