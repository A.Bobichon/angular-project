import { User } from 'src/app/Entity/user';

export interface Operation {
  id?:number;
  montant:number;
  tag:string;
  datetime:Date;
  user?:User[];
}
