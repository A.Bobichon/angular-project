import { Component, OnInit } from '@angular/core';
import { User } from '../Entity/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-form-inscription',
  templateUrl: './form-inscription.component.html',
  styleUrls: ['./form-inscription.component.css']
})
export class FormInscriptionComponent implements OnInit {
  
  userAdd:User = { name: null, password: null, solde: null };

  constructor(private service:UserService) { }

  ngOnInit() {
  }

  add(userAdd){
    this.service.addUser(userAdd)
    .subscribe( responce => this.userAdd = responce);
  }

}
