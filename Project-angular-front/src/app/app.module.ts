import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';
import { FormsModule } from "@angular/forms";
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ApiInterfaceComponent } from './api-interface/api-interface.component';
import { FormComponent } from './api-interface/form/form.component';
import { ListeTransComponent } from './api-interface/liste-trans/liste-trans.component';
import { FormInscriptionComponent } from './form-inscription/form-inscription.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { GraphComponent } from './api-interface/graph/graph.component';
import { ChartsModule } from 'ng2-charts';

const routes:Routes = [
  { path: '', component: HomeComponent },
  { path: 'apiInterface', component: ApiInterfaceComponent },
  { path: 'inscription', component: FormInscriptionComponent },
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', component :NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    FooterComponent,
    ApiInterfaceComponent,
    FormComponent,
    ListeTransComponent,
    FormInscriptionComponent,
    NotFoundComponent,
    GraphComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      routes,
      {enableTracing: true}
    ),
    HttpClientModule,
    FormsModule,
    ChartsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
